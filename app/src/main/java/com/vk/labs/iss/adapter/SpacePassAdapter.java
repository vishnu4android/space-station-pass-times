package com.vk.labs.iss.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vk.labs.iss.R;
import com.vk.labs.iss.apimodel.Response;
import com.vk.labs.iss.utils.Utility;

import java.util.List;

/**
 * Created by vkonakalla on 12/20/17.
 */

public class SpacePassAdapter extends RecyclerView.Adapter<SpacePassAdapter.MyViewHolder> {

    Context context;
    List<Response> list;

    public SpacePassAdapter(Context context, List<Response> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pass_row, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(itemView);


        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        long timestamp = list.get(position).getRisetime();
        holder.mDateText.setText("" + Utility.getDate(timestamp));
        holder.mSecondsText.setText("" + list.get(position).getDuration());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mSecondsText;
        TextView mDateText;

        public MyViewHolder(View itemView) {
            super(itemView);
            mSecondsText = (TextView) itemView.findViewById(R.id.secondsTextView);
            mDateText = (TextView) itemView.findViewById(R.id.dateTextView);
        }
    }
}
