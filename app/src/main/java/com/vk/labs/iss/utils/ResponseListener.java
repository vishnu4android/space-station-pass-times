package com.vk.labs.iss.utils;

/**
 * Created by vishnu.konakalla on 6/20/2016.
 */
public interface ResponseListener<T>
{
    public void getResult(T object);
}
