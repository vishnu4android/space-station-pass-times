package com.vk.labs.iss.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vishnu.konakalla on 6/20/2016.
 */

public class VolleyHelper {
    private static final String TAG = "VolleyHelper";
    private static VolleyHelper instance = null;

    //for Volley API
    public RequestQueue requestQueue;

    private VolleyHelper(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        //other stuf if you need
    }

    public static synchronized VolleyHelper getInstance(Context context) {
        if (null == instance)
            instance = new VolleyHelper(context);
        return instance;
    }

    //this is so you don't need to pass context each time
    public static synchronized VolleyHelper getInstance() {
        if (null == instance) {
            throw new IllegalStateException(VolleyHelper.class.getSimpleName() +
                    " is not initialized, call getInstance(...) first");
        }
        return instance;
    }

    public void getRequest(String url, JSONObject jsonObject, final ResponseListener<JSONObject> listener) {

        Log.i(TAG, "************************************************");
        Log.i(TAG, "Get Request URL: " + url);
        Log.i(TAG, "################################################");

        JsonObjectRequest objRequest = new JsonObjectRequest(Request.Method.GET, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG + ": ", "somePostRequest Response : " + response.toString());
                        if (null != response.toString())
                            listener.getResult(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (null != error.networkResponse) {
                            Log.d(TAG + ": ", "Error Response code: " + error.networkResponse.statusCode);
                            listener.getResult(new JSONObject());
                        } else {
                            listener.getResult(new JSONObject());
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                if (statusCode >= 204)
                    listener.getResult(new JSONObject());
                Log.d("VK_Debug" + ": ", "response is: " + response.statusCode);
                return super.parseNetworkResponse(response);
            }
        };

        requestQueue.add(objRequest);
    }
}