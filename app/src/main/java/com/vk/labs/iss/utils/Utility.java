package com.vk.labs.iss.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.vk.labs.iss.BuildConfig;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class Utility {

    private static final String TAG = Utility.class.getCanonicalName();
    public static final int NO_INTERNET_CONNECTION = 1;
    public static final int NO_GPS_ACCESS = 2;
    private static final int CONNECTION_TIMEOUT = 25000;
    private static Intent intent;
    public static Toast toast;
    private static ProgressDialog progressDialog;


    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .getState() == NetworkInfo.State.CONNECTED
                    || connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .getState() == NetworkInfo.State.CONNECTING) {
                return true;
            } else if (connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .getState() == NetworkInfo.State.CONNECTED
                    || connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .getState() == NetworkInfo.State.CONNECTING) {

                return true;
            } else
                return false;
        } catch (Exception e) {
            logException(TAG, e);
            return false;
        }
    }

    public static void showToastMessage(Context context, String message) {
        try {
            if (!isValueNullOrEmpty(message) && context != null) {

                if (toast != null)
                    toast.cancel();

                toast = Toast.makeText(
                        context.getApplicationContext(), message,
                        Toast.LENGTH_LONG);
                toast.show();
            }
        } catch (Exception e) {
            logException(TAG, e);
        }
    }


    public static void showLog(String logMsg, String logVal) {
        try {
            if (ApiConstants.logMessageOnOrOff) {
                if (!isValueNullOrEmpty(logMsg) && !isValueNullOrEmpty(logVal)) {
                    Log.i(logMsg, logVal);
                }
            }
        } catch (Exception e) {
            logException(TAG, e);
        }
    }



    public static boolean isValueNullOrEmpty(String value) {
        boolean isValue = false;
        if (value == null || value.equals(null) || value.equals("") || value.equals(".")
                || value.equals("null") || value.equals("0") || value.trim().length() == 0) {
            isValue = true;
        }
        return isValue;
    }



    public static void logException(String fileName, Exception e) {
        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug"))
            Log.e("Got exception in: ", fileName, e);
    }


    public static void closeKeyboard(FragmentActivity fragmentActivity) {
        try {
            //
            InputMethodManager inputManager = (InputMethodManager)
                    fragmentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow((null == fragmentActivity.getCurrentFocus()) ? null : fragmentActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            ///
        } catch (Exception e) {

        }

    }


    public static String getDate(long timeStamp){

        Calendar calendar = Calendar.getInstance();
        TimeZone tz = TimeZone.getDefault();
        calendar.add(Calendar.MILLISECOND,
                tz.getOffset(calendar.getTimeInMillis()));
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
        java.util.Date currenTimeZone=new java.util.Date((long)timeStamp * 1000);
        return sdf.format(currenTimeZone);
    }

    public static void showProgressDialog(Context mContext, String msg) {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                return;

            progressDialog = new ProgressDialog(mContext);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(msg);
            progressDialog.show();
        } catch (Exception e) {
            Utility.showLog("ERROR", e.toString());
        }
    }

    public static void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        } catch (Exception e) {
            Utility.showLog("ERROR", e.toString());
        }
    }

}
