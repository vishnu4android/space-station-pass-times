package com.vk.labs.iss.utils;


public class ApiConstants {
    public static final String BASE_URL = "http://api.open-notify.org/iss-pass.json?";//Main url
    public static final String LATITUDE = "lat=";
    public static final String LONGITUDE = "&lon=";
    public static final String NUMBER = "&n=";
    public static final boolean logMessageOnOrOff = true;
}

