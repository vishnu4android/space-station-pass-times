package com.vk.labs.iss.application;

import android.app.Application;

import com.vk.labs.iss.utils.VolleyHelper;


//AppController
public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        VolleyHelper.getInstance(this);
    }

}
