
package com.vk.labs.iss.apimodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SpacePassResponse {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("request")
    private Request mRequest;
    @SerializedName("response")
    private List<Response> mResponse;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Request getRequest() {
        return mRequest;
    }

    public void setRequest(Request request) {
        mRequest = request;
    }

    public List<Response> getResponse() {
        return mResponse;
    }

    public void setResponse(List<Response> response) {
        mResponse = response;
    }

}
